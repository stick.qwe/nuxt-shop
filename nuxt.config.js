require('dotenv').config()

module.exports = {
  /*
  ** Headers of the page
  */
  head: {
    title: 'codecourse-e-com',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Nuxt.js project' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },
  server: {
    port: process.env.DEPLOYMENT_PORT,
  },
  css: [
    '~assets/styles/app.scss'
  ],


  modules: [
    '@nuxtjs/axios',
    '@nuxtjs/auth'
  ],

  axios: {
    baseURL: process.env.URL_API,
  },
  /*
  ** Customize the progress bar color
  */
  loading: { color: '#3B8070' },
  /*
  ** Build configuration
  */

 auth: {
  strategies: {
    password_grant: {
      _scheme: 'local',
      endpoints: {
        login: {
          url: process.env.SECURITY_URL + '/login',
          method: 'post',
          propertyName: 'access_token'
        },
        logout: false,
        user: {
          url: '/user'
        }
      }
    }
  },
  redirect: {
    login: '/auth/login',
    home: '/'
  }
},
  build: {

    /*
    ** Run ESLint on save
    */
    extend(config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  }
}

